# README #

### What is this repository for? ###

This project is a solution for below given programming task.

You are given a list of comma separated accession numbers as input.

An accession number has the following format: L...LN...N (e.g. AB1234)

where L...L denotes one or more ASCII7 letters and N denotes one or more digits (0,1,2,3,4,5,6,7,8 or 9).

Please return an ordered list of accession numbers where any consecutive digits N...N that share the same prefix L...L are collapsed into an accession number range.

An accession number range has the following format : L...LN...N-L...LN...N (e.g. A00001-A00005).

Please note that the N...N digits are left padded using 0s (e.g. 00001) and that the length of the N...N digits must be the same for accession numbers to be part of the same accession number range.

Example input:
A00000, A0001, ERR000111, ERR000112, ERR000113, ERR000115, ERR000116, ERR100114, ERR200000001, ERR200000002, ERR200000003, DRR2110012, SRR211001, ABCDEFG1

Expected output:
A00000, A0001, ABCDEFG1, DRR2110012, ERR000111-ERR000113, ERR000115-ERR000116, ERR100114, ERR200000001-ERR200000003, SRR211001 


### Other Instructions ###
	Please create a Java 8 project using standard maven directory layout.
	Please build your project using gradle or maven.
	Please provide instructions in README.md file formatted using GitHub markdown syntax.
	Please unit test the functionality.
	Please provide a command line endpoint which accepts a list of accession numbers as input and returns an output as described below.

Optional: 
	Please provide a web endpoint using an embedded web server which accepts a list of accession numbers as input and returns an output as described below.
	Please implement the following functionality preferably using the Java Stream API:


### How do I get set up? ###

Clone "AccessionNumbersApp" repository at your local machine
Import "AccessionNumbersApp" project as Maven Existing project in your Eclipse. I am using "Spring Tool Suite" Version: 3.8.4.RELEASE.
Use maven and execute Clean and Install commands.
We can run project in two ways i.e. Either as a Java Application or as a Web Application
1. Running aa Java Application
Just run "Main.java" as a Java Application. It will ask for numbers on console. 
Give comman separated numbers in one line and press "Enter". Here is your result.

NOTE: Make sure that in AccessionNumbersAppConfiguration.java, @Configuration and @EnableWebMvc are commented.

2. Running aa Web Application (RESTFUL API)
In AccessionNumbersAppConfiguration.java uncomment @Configuration and @EnableWebMvc.
Run your project on server. I am using apache-tomcat-8.0.30.
Now, you may call following RESTFUL API. Same is given as POSTMAN in Source folder.

URL: http://localhost:9090/AccesssionNumbersApp/accessionnumbers/sort
Body:
{
"accessionNumbers": "A00000, A0001, ERR000111, ERR000112, ERR000113, ERR000115, ERR000116, ERR100114, ERR200000001, ERR200000002, ERR200000003, DRR2110012, SRR211001, ABCDEFG1, ERR200000006, ERR200000004, ERR200000009, ERR200000005"
}	
Header: Content-Type: application/json


### Unit Testing ###
There is one JUnit test.
You may run the same by right clicking on AccessionNumbersTest.java file and "Run as JUnit Test".

### Contribution guidelines ###

Do let me know in case of any error.

Feel free to add your work.

### Who do I talk to? ###

* Krishan Babbar: krishan.babbar@gmail.com
