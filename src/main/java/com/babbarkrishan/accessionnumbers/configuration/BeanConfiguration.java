package com.babbarkrishan.accessionnumbers.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@ComponentScan(basePackages = { "com.babbarkrishan.accessionnumbers*" })
public class BeanConfiguration {

	@Autowired
	Environment env;

	public BeanConfiguration() {
	}
}
