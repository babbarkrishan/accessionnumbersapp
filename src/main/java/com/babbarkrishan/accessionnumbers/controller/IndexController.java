package com.babbarkrishan.accessionnumbers.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.babbarkrishan.accessionnumbers.bean.AccessionNumbers;
import com.babbarkrishan.accessionnumbers.service.AccessionNumbersService;

@RestController
public class IndexController {
	private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

	@Autowired
	private AccessionNumbersService accessionNumbersService;

	@RequestMapping(method = RequestMethod.GET)
	public String getIndexPage() {
		return "Dashboard";
	}

	@RequestMapping(value = "/accessionnumbers/sort", method = RequestMethod.POST)
	public ResponseEntity<String> sortAccessionsNumbers(@RequestBody AccessionNumbers accessionNumbers) {
		logger.debug("User given Accession Numbers is: {}", accessionNumbers);
		String sortedAccessionNumbers = "";
		if (StringUtils.isNotEmpty(accessionNumbers.getAccessionNumbers())) {
			sortedAccessionNumbers = accessionNumbersService
					.processAccessionNumbers(accessionNumbers.getAccessionNumbers());
		}

		return new ResponseEntity<String>(sortedAccessionNumbers, HttpStatus.OK);
	}

}