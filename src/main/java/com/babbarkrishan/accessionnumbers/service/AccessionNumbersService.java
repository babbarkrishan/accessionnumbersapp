package com.babbarkrishan.accessionnumbers.service;

import java.util.List;

public interface AccessionNumbersService {
	
	public List<String> sortAccessionNumbers(List<String> accessionNumbers);

	public String getSortedAccessionNumbersWithRange(List<String> sortedAccessionNumbers);
	
	public String processAccessionNumbers(String inputAccessionNumbers);
	
}
