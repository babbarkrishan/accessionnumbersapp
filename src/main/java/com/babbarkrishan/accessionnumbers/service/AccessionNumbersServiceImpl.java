package com.babbarkrishan.accessionnumbers.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("accessionNumbersService")
public class AccessionNumbersServiceImpl implements AccessionNumbersService {

	private static final Logger logger = LoggerFactory.getLogger(AccessionNumbersServiceImpl.class);

	@Override
	public String processAccessionNumbers(String inputAccessionNumbers) {
		List<String> inputAccessionNumbersList = Arrays.asList(inputAccessionNumbers.split("\\s*,\\s*"));
		List<String> sortedAccessionNumbers = this.sortAccessionNumbers(inputAccessionNumbersList);
		return this.getSortedAccessionNumbersWithRange(sortedAccessionNumbers);
	}

	@Override
	public List<String> sortAccessionNumbers(List<String> accessionNumbers) {
		logger.debug("sortAccessionNumbers: Input Numbers - {}", accessionNumbers);

		List<String> sortedAccessionNumbers = new ArrayList<String>(accessionNumbers.size());

		Comparator<String> byCharsNNumbers = (an1, an2) -> an1.compareTo(an2);

		sortedAccessionNumbers = accessionNumbers.stream().sorted(byCharsNNumbers).collect(Collectors.toList());

		return sortedAccessionNumbers;
	}

	@Override
	public String getSortedAccessionNumbersWithRange(List<String> sortedAccessionNumbers) {
		StringBuilder resultWithRange = new StringBuilder();

		int prevNumber = -1;
		boolean isRangeFound = false;
		String rangeLastANumber = "";

		for (String aAccessionNumber : sortedAccessionNumbers) {
			int aNumber = Integer.valueOf(aAccessionNumber.replaceAll("[^\\d.]", ""));
			if (prevNumber > 0 && (++prevNumber == aNumber)) {
				isRangeFound = true;
				rangeLastANumber = aAccessionNumber;
				continue;
			} else if (isRangeFound) {
				prevNumber = aNumber;
				resultWithRange.append("-");
				resultWithRange.append(rangeLastANumber);
				resultWithRange.append(", ");
				resultWithRange.append(aAccessionNumber);
				isRangeFound = false;
			} else {
				prevNumber = aNumber;
				resultWithRange.append(", ");
				resultWithRange.append(aAccessionNumber);
			}
		}

		return resultWithRange.substring(2, resultWithRange.length());
	}

}