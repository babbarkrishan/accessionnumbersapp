package com.babbarkrishan.configuration;

import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.babbarkrishan.accessionnumbers.configuration.BeanConfiguration;
import com.babbarkrishan.accessionnumbers.service.AccessionNumbersService;

public class Main {

	public static void main(String sss[]) throws Exception {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BeanConfiguration.class);
		AccessionNumbersService accessionNumbersService = (AccessionNumbersService) context
				.getBean("accessionNumbersService");

		System.out.println("Enter your accession numbers separated by comma.");
		Scanner sc = new Scanner(System.in);
		String inputAccessionNumbers = sc.nextLine();
		sc.close();

		String sortedAccessionNumbers = "";
		if (StringUtils.isNotEmpty(inputAccessionNumbers)) {
			sortedAccessionNumbers = accessionNumbersService.processAccessionNumbers(inputAccessionNumbers);
		}

		System.out.println(sortedAccessionNumbers);
	}
}
