package com.babbarkrishan.accessionnumbers.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.babbarkrishan.accessionnumbers.configuration.BeanConfiguration;
import com.babbarkrishan.accessionnumbers.service.AccessionNumbersService;

public class AccessionNumbersTest {

	
	@Test
	public void testAccessionNumbersList() {
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BeanConfiguration.class);
		AccessionNumbersService accessionNumbersService = (AccessionNumbersService) context
				.getBean("accessionNumbersService");
		
		String inputAccessionNumbers = "A00000, A0001, ERR000111, ERR000112, ERR000113, ERR000115, ERR000116, ERR100114, ERR200000001, ERR200000002, ERR200000003, DRR2110012, SRR211001, ABCDEFG1";
        String expectedOutputAccessionNumbers = "A00000, A0001, ABCDEFG1, DRR2110012, ERR000111-ERR000113, ERR000115-ERR000116, ERR100114, ERR200000001-ERR200000003, SRR211001";
        
		String sortedAccessionNumbers = accessionNumbersService.processAccessionNumbers(inputAccessionNumbers);
		
        assertEquals(sortedAccessionNumbers, expectedOutputAccessionNumbers);
	}

}
